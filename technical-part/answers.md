***1. How long did you spend on the coding test below? What would you add to your solution if you had more time? If you didn't spend much time on the coding test then use this as an opportunity to explain what you would add.***


I have spent around 25 hours on the coding test.

If I had more time, I would have added the following items
- More tests and increase coverage
- Appropiate way to represent enum
- Validation for Metadata field
- More efficient solution for ranking update
- Store audit logs with more fields
- orchestrate the launch environment in AWS using CloudFormation

----


***2. What was the most useful feature that was added to the latest version of your chosen language? Please include a snippet of code that shows how you've used it.***

- Python3/Django

In latest version of Python 3.6, Formatted string literals are prefixed with 'f' and are similar to the format strings accepted by str.format(). Here is one of snippet where I used 'f' formatting

```python
log = {datetime.now().__str__(): f"Item #{self.id} has been updated!"}
```

- JavaScript/Vue

Latest ES6 version, the arrow function was introduced. Here is one of the snippet where I used arrow function. 

```js
 this.form.formInline.forEach(element => {
    this.form.metadata[element.key] = {"type": element.type, "value": element.value}
});
```


---

***3. How would you track down a performance issue in production? Have you ever had to do this?***

- Usually, A monitoring tool like newrelic help to track down performance issue. However, it is also possible to track down performace issue manually. Example: Profiling

- yes. Had happened couple of times. Recently, In my current company we faced High CPU/Memory usages issue with Celery and RabbitMQ, and fixed it. I wrote [blog](https://medium.com/@alaminopu.me/solving-rabbitmq-high-cpu-memory-usages-problem-with-celery-d4172ba1c6b3) about it.
