
![pipeline](https://gitlab.com/alaminopu/favorite-things/badges/development/pipeline.svg?job=pipeline)
![coverage](https://gitlab.com/alaminopu/favorite-things/badges/master/coverage.svg?job=coverage)

# Favorite Things

The project is an application that allows the user to track their favorite things.



## Database Design

To ensure users can be able add more categories, category table kept separated and it has one to many relationship with the item table.

Metadata of items is a JSONField() as metadata stores key/value. The Audit Log field is also JSONField(). It stores audit log data as a list of dictionary -- key(date) and value(action) 


![alt text](ERD.png "ERD")


## Getting Started


### Prerequisites

- Docker
- Docker Compose

### Quick Install

- Clone the repo
- cd into the project directory
- Run `./build.sh`

### Manual Installation

- Clone the repo
- cd into the project directory

Run the following command

```bash
docker-compose up --build
```

On another terminal, run the following commands

```bash
docker-compose exec backend bash
```

Then `cd` into the `source` directory. Run migration command

```bash
python manage.py migrate
```

Load default categories using the following command

```bash
python manage.py loaddata category-fixture.json
```


## Running the tests

By Keeping docker containers running, on another terminal, run the following commands.

```
$ docker-compose exec backend bash
# cd source
# pytest -v
```

## Production Deployment

### Prerequisites

- `pip3`
- `aws-cli`
- AWS Credentials
- S3 Buckets 
- RDS Postgres Instance

#### Backend 

Run the following scripts to deploy django backend service to production using zappa on AWS Lambda

```bash
./deploy-backend.sh
```

**Note: Before deploying backend, make necessary changes to the zappa_settings.json file. or Run `zappa init` to create new settings. For Security reason, few environment values are omitted from the repo**

#### Frontend

```bash
./deploy-frontend.sh
```

**Note: Before deploying frontend, please update backend url in App.vue file**

## Links 
- [Deployed Frontend URL](http://britecore-test.s3-website-ap-southeast-1.amazonaws.com/)
- [Deployed Backend URL](https://g98cgqe1t1.execute-api.ap-southeast-1.amazonaws.com/production)

Note: For backend, may need to go to specific api endpoints.


## API Endpoints

#### GET /api/v1/categories/

Returns a list of category data.


Sample response:
```json
[
    {
        "id": 1,
        "name": "person"
    },
    {
        "id": 2,
        "name": "place"
    },
    {
        "id": 3,
        "name": "food"
    },
    {
        "id": 4,
        "name": "cloth"
    }
]
```


#### POST /api/v1/categories/

Endpoint for creating a category.


Sample Request
```json
{
	"name": "mountain"
}
```


Sample Response
```json
{
    "id": 5,
    "name": "mountain"
}
```

#### GET /api/v1/items/

Returns a list of favorite things.


Sample response:
```json
[
    {
        "id": 2,
        "category": {
            "id": 2,
            "name": "place"
        },
        "created_at": "2019-07-03T16:30:00.998163Z",
        "modified_at": "2019-07-03T16:30:00.998207Z",
        "title": "Bandarban",
        "description": "A very beautiful place in Bangladesh",
        "ranking": 1,
        "metadata": {
            "journey_date": {
                "type": "date",
                "value": "2019-07-21T18:00:00.000Z"
            }
        },
        "audit_log": [
            {
                "2019-07-03 16:30:00.997921": "A new item has been added!"
            }
        ]
    }
]
```


#### POST /api/v1/items/

Endpoint for creating a favorite thing.


Sample Request
```json
{
    "title": "Pizza",
    "description": "Pizza Four Season",
    "category": 3,
    "ranking": 2,
    "metadata": {
        "shop": {
            "type": "string",
            "value": "Bhooter Bari"
        }
    }
}
```


Sample Response
```json
{
    "id": 4,
    "category": {
        "id": 3,
        "name": "food"
    },
    "created_at": "2019-07-03T18:29:41.607698Z",
    "modified_at": "2019-07-03T18:29:41.607733Z",
    "title": "Pizza",
    "description": "Pizza Four Season",
    "ranking": 2,
    "metadata": {
        "shop": {
            "type": "string",
            "value": "Bhooter Bari"
        }
    },
    "audit_log": [
        {
            "2019-07-03 18:29:41.607512": "A new item has been added!"
        }
    ]
}
```

#### PUT /api/v1/items/{id}/

Endpoint for updating a favorite thing.

Sample Request
```json
{
    "title": "Pizza",
    "description": "Fantastic Pizza",
    "category": 3,
    "ranking": 2,
    "metadata": {
        "shop": {
            "type": "string",
            "value": "Bhooter Bari"
        },
        "visit_count": {
            "type": "number",
            "value": "13"
        }
    }
}
```

Sample Response
```json
{
    "id": 4,
    "category": {
        "id": 3,
        "name": "food"
    },
    "created_at": "2019-07-03T18:29:41.607698Z",
    "modified_at": "2019-07-03T18:33:04.498461Z",
    "title": "Pizza",
    "description": "Fantastic Pizza",
    "ranking": 2,
    "metadata": {
        "shop": {
            "type": "string",
            "value": "Bhooter Bari"
        },
        "visit_count": {
            "type": "number",
            "value": "13"
        }
    },
    "audit_log": [
        {
            "2019-07-03 18:29:41.607512": "A new item has been added!"
        },
        {
            "2019-07-03 18:33:04.498328": "Item #4 has been updated!"
        }
    ]
}
```

## Others

The quiz file and answers of technical questions files are linked below 

- [quiz.py](https://gitlab.com/alaminopu/favorite-things/blob/master/quiz.py)
- [myself.json](https://gitlab.com/alaminopu/favorite-things/blob/master/technical-part/myself.json)
- [answers.md](https://gitlab.com/alaminopu/favorite-things/blob/master/technical-part/answers.md)