# up and running using docker
docker-compose up -d 

# wait for postgres 
sleep 30

# migration
docker-compose exec backend bash -c 'cd source && python manage.py migrate && python manage.py loaddata category-fixture.json'

# down and up again
docker-compose down 
docker-compose up