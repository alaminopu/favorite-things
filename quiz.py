from cryptography.fernet import Fernet

key = 'TluxwB3fV_GWuLkR1_BzGs1Zk90TYAuhNMZP_0q4WyM='

# Oh no! The code is going over the edge! What are you going to do?
message = b'gAAAAABdFeRDWtpOlmiF3olxOPQc-EveCR8bLh9H61am1d6kGyNzF-NmqWo3hxiVxKBqO0bHivc01iAHoP1sianp3Vlo8aFRnL7yhlMbXQVJ7EQ6-hT76a6bV8gpcijsfFMMXLbQRWSBAH8c4l1odNxi43zcfMblSO_760hnLH4Sf7lmzHNPJIk='

def main():
    f = Fernet(key)
    print(f.decrypt(message))


if __name__ == "__main__":
    main()