# Set S3 Bucket name
S3_BUCKET_NAME='britecore-test'

# go to frontend directory 
cd frontend

# install npm packages 
npm install

# build with npm
npm run build 

# Upload 
aws s3 sync --acl public-read --delete ./dist s3://$S3_BUCKET_NAME