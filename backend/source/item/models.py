from datetime import datetime
from django.contrib.postgres.fields import JSONField
from django.db import models, IntegrityError
from django.core.validators import MinLengthValidator
from django.db.models import F, UniqueConstraint


# Create your models here.
class BaseModel(models.Model):
    """
    It's a abstract model for created_at and modified_at field
    """
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Category(BaseModel):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class Item(BaseModel):
    title = models.CharField(max_length=255)
    description = models.TextField(
        blank=True,
        null=True,
        validators=[MinLengthValidator(10, message="Must at least 10 characters long")]
    )
    ranking = models.IntegerField()
    metadata = JSONField()
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    audit_log = JSONField(default=list)

    class Meta:
        constraints = [UniqueConstraint(fields=['ranking', 'category'], name='category_ranking')]

    
    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        # audit log
        if self.id:
            # Todo:: Track field changes
            log = {datetime.now().__str__(): f"Item #{self.id} has been updated!"}
            self.audit_log.append(log)
        else:
            self.audit_log = []
            log = {datetime.now().__str__(): f"A new item has been added!"}
            self.audit_log.append(log)

        try:
            return super().save(*args, **kwargs)
        except IntegrityError:
            # An item with same ranking exists in this category
            self.update_ranking()
            return super().save(*args, **kwargs)


    def update_ranking(self):
        """ Update ranking of the items in same category """
        category_obj = (Item.objects.all().filter(
            category=self.category
        ).exclude(id=self.id))

        if category_obj.filter(ranking=self.ranking).exists():
            categories_rank = category_obj.filter(
                ranking__gte=self.ranking
            ).order_by(
                '-ranking'
            )

            for ob in categories_rank:
                ob.ranking = F('ranking') + 1
                ob.save()

