
from rest_framework import serializers

from .models import Category, Item
from .fields import PresentablePrimaryKeyRelatedField


class JSONSerializerField(serializers.Field):
    """ Serializer for JSONField -- required to make field writable"""
    def to_internal_value(self, data):
        return data
    def to_representation(self, value):
        return value


class CategorySerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Category
        fields = ("id", "name")


class ItemSerializer(serializers.ModelSerializer):
    metadata = JSONSerializerField(allow_null=True)
    category = PresentablePrimaryKeyRelatedField(
        presentation_serializer=CategorySerializer,
        queryset=Category.objects.all()
    )

    class Meta:
        model = Item
        fields = '__all__'
