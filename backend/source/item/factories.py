import factory
import json
from faker import Faker
from django.core.serializers.json import DjangoJSONEncoder

from .models import Item, Category

faker = Faker("en_US")


class CategoryFactory(factory.DjangoModelFactory):
    """
        Category Factory
    """
    class Meta:
        model = Category
    name = faker.pystr(max_chars=20)


class ItemFactory(factory.DjangoModelFactory):
    """
        Item Factory
    """
    class Meta:
        model = Item
    
    title = faker.pystr(max_chars=50)
    description = faker.text()
    ranking = faker.pyint(min=1, max=9999)
    category = factory.SubFactory(CategoryFactory)
    metadata = json.dumps(
        faker.pydict(nb_elements=3, variable_nb_elements=True),
        sort_keys=True,
        indent=4,
        cls=DjangoJSONEncoder
    )