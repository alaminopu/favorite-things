
from django.urls import path

from .views import ItemListCreateView, ItemRetrieveView, CategoryListCreateView

app_name='item'

urlpatterns = [
    path("items/", ItemListCreateView.as_view(), name="item-list"),
    path("items/<int:pk>/", ItemRetrieveView.as_view(), name="item-retrieve"),
    path("categories/", CategoryListCreateView.as_view(), name="category-list"),
]