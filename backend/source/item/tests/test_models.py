import pytest
from item.factories import ItemFactory, CategoryFactory


@pytest.mark.django_db
class TestItem(object):
    @pytest.mark.parametrize(
        'ranking',
        [2, 3, 5]
    )
    def test_ranking(self, ranking, item):
        item.ranking = ranking
        item.save()
        assert item.ranking == ranking

    def test_category_ranking(self, ranking, category, item):
        item.ranking = ranking
        item.category = category
        item.save()
        assert item.ranking == ranking 
        assert item.category == category


@pytest.mark.django_db
class TestCategory(object):
    def test_category_creation(self):
        """Test category creation"""
        category = CategoryFactory(name="food")
        assert category.name == "food"
