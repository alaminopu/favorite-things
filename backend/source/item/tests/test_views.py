import pytest
from item.factories import ItemFactory, CategoryFactory

from faker import Faker
from django.urls import reverse
from item.models import Item

faker = Faker("en_US")


@pytest.mark.django_db
class TestItemView(object):
    url = reverse("item:item-list")

    def test_list_items(self, client):
        response = client.get(self.url)
        assert response.status_code == 200

    def test_list_items_count(self, client):
        response = client.get(self.url)
        total = Item.objects.all().count()
        assert total == len(response.data)
    
    def test_create_item(self, client, item_dict):
        response = client.post(self.url, item_dict)
        assert response.status_code == 201

    

@pytest.mark.django_db
class TestCategoryView(object):
    url = reverse("item:category-list")

    def test_list_category(self, client):
        response = client.get(self.url)
        assert response.status_code == 200

    def test_create_category(self, client, category_dict):
        response = client.post(self.url, category_dict)
        assert response.status_code == 201