from rest_framework.generics import ListCreateAPIView, RetrieveUpdateAPIView, CreateAPIView

from .models import Item, Category
from .serializers import ItemSerializer, CategorySerializer


class CategoryListCreateView(ListCreateAPIView):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()


class ItemListCreateView(ListCreateAPIView):
    serializer_class = ItemSerializer
    queryset = Item.objects.all()

    def get_queryset(self):
        return Item.objects.all().order_by("ranking")


class ItemRetrieveView(RetrieveUpdateAPIView):
    serializer_class = ItemSerializer
    queryset = Item.objects.all()