import pytest
import factory
from faker import Faker

from item.factories import ItemFactory, CategoryFactory



faker = Faker("en_US")

@pytest.fixture()
def category():
    category = CategoryFactory()
    return category

@pytest.fixture()
def category_dict():
    data = factory.build(dict, FACTORY_CLASS=CategoryFactory)
    return data


@pytest.fixture()
def item(category):
    item = ItemFactory(
        category=category
    )
    return item

@pytest.fixture()
def items(category):
    items = ItemFactory.create_batch(4)
    return items


@pytest.fixture()
def item_dict():
    data = factory.build(dict, FACTORY_CLASS=ItemFactory)
    category = CategoryFactory()
    data['category'] = category.id
    return data


@pytest.fixture()
def ranking():
    return faker.pyint(min=1, max=9999)