# Copy zappa configuration file to backned/source
cp zappa_settings.json backend/source/

# Go to backend
cd backend

# copy requirements.txt into source file 
cp requirements.txt source/

# Go to source directory 
cd source 

# Install virtualenv
pip3 install virtualenv

# create virtualenv
virtualenv venv

# activate virtualenv
source venv/bin/activate

# Installing zappa
pip install zappa 

# installing other depended packages 
pip install -r requirements.txt 

# Deploy production
zappa deploy production 

# Migration
zappa manage production migrate

# load data 
zappa manage production loaddata category-fixture.json

# see status 
zappa status production

# Deactivate virtualevn
deactivate 